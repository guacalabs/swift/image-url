// swift-tools-version:5.1
import PackageDescription

let package = Package(
    name: "ImageURL",
    platforms: [.iOS(.v13), .tvOS(.v12), .macOS(.v10_15)],
    products: [
        .library(
            name: "ImageURL",
            targets: ["ImageURL"]
        )
    ],
    dependencies: [],
    targets: [
        .target(
            name: "ImageURL",
            dependencies: []
        )
    ],
    swiftLanguageVersions: [.v5]
)

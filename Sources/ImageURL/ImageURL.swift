import SwiftUI
import Combine

// MARK: - ImageURL

public struct ImageURL: View {
    @ObservedObject private var loader: ImageURL.Loader

    private let url: URL?
    private let placeholder: Image
    private let contentMode: ContentMode

    public init(path: String, placeholder: Image, contentMode: ContentMode = .fit) {
        self.init(url: URL(string: path), placeholder: placeholder)
    }

    public init(url: URL?, placeholder: Image, contentMode: ContentMode = .fit) {
        self.url = url
        self.placeholder = placeholder
        self.loader = .init(imageURL: url)
        self.contentMode = contentMode
    }

    public var body: some View {
        if let image = loader.image {
            return image
                .resizable()
                .aspectRatio(contentMode: contentMode)
                .onAppear(perform: {})
                .onDisappear(perform: {})
        } else {
            return placeholder
                .resizable()
                .aspectRatio(contentMode: contentMode)
                .onAppear(perform: self.loader.load)
                .onDisappear(perform: self.loader.cancel)
        }
    }
}

// MARK: - ImageURL - Loader

extension ImageURL {
    public final class Loader: ObservableObject {
        @Published public private(set) var image: Image?

        private let imageURL: URL?
        private let session: URLSession

        public init(imageURL: URL?, session: URLSession = .shared) {
            self.imageURL = imageURL
            self.session = session
        }

        private var disposables: Set<AnyCancellable> = []
    }
}

public extension ImageURL.Loader {
    func load() {
        guard let imageURL = imageURL else {
            return
        }
        session
            .dataTaskPublisher(for: imageURL)
            .subscribe(on: DispatchQueue.global())
            .map(\.data)
            .receive(on: DispatchQueue.main)
            .sink(receiveCompletion: { _ in }, receiveValue: { [weak self] imageData in
                #if canImport(UIKit)
                guard let self = self, let uiImage = UIImage(data: imageData) else {
                    return
                }
                self.image = Image(uiImage: uiImage)
                #elseif canImport(AppKit)
                guard let self = self, let nsImage = NSImage(data: imageData) else {
                    return
                }
                self.image = Image(nsImage: nsImage)
                #endif
            })
            .store(in: &disposables)
    }

    func cancel() {
        disposables
            .forEach({ $0.cancel() })
    }
}
